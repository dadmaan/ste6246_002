import numpy as np
import random
from collections import deque
import matplotlib.pyplot as plt
from tqdm import tqdm
from tabulate import tabulate
import time
import itertools as itr

EPOCH = 100
div = "="*40
TIME_DELAY = 0

class Model1():
    def __init__(self,p1,p2):
        self.good_combination = np.array([])
        self.passenger1 = p1
        self.passenger2 = p2
        self.max_load = 500
        self.history = deque()
        self.reward = 0
        self.episode_reward = []

    def Start(self, epoch):
        p1_lugg = self.possibleCombinations(passenger1.luggage)
        p2_lugg = self.possibleCombinations(passenger2.luggage)
        luggage = zip(p1_lugg,p2_lugg)

        for episode in tqdm(range(epoch)):
            time.sleep(TIME_DELAY)
            for lugg1, lugg2 in luggage:
                self.evaluate([lugg1,lugg2])
            self.episode_reward.append(self.reward)
        return self.history,self.reward,self.episode_reward

    def evaluate(self,luggage):
        p1_weight = np.sum(luggage[0]) + passenger1.passenger_weight
        p2_weight = np.sum(luggage[1]) + passenger2.passenger_weight
        totall = p1_weight+p2_weight
        if(totall <= self.max_load):
            self.history.append(luggage)
            self.reward = self.reward + 100
        else:
            self.reward = self.reward -1000

    def possibleCombinations(self, luggage):
        return sum([list(map(list, itr.combinations(luggage, i))) for i in range(len(luggage) + 1)], [])

class Model2():
    def __init__(self,p1,p2):
        self.good_combination = np.array([])
        self.passenger1 = p1
        self.passenger2 = p2
        self.max_load = 500
        self.success = deque()
        self.failure = deque()
        self.reward = 0
        self.episode_reward = []
        self.batch_size = 3

    def Start(self, epoch):
        for episode in tqdm(range(epoch)):
            time.sleep(TIME_DELAY)
            self.train()
            self.episode_reward.append(self.reward)
        return self.success, self.reward,self.episode_reward

    def train(self):
        samples = self.selectLuggage()
        for lugg1,lugg2 in samples:
            if [lugg1,lugg2] not in self.failure:
                self.evaluate([lugg1,lugg2])
        return self.reward/self.batch_size

    def evaluate(self,luggage):
        p1_weight = np.sum(luggage[0]) + passenger1.passenger_weight
        p2_weight = np.sum(luggage[1]) + passenger2.passenger_weight
        totall = p1_weight+p2_weight
        if(totall <= self.max_load):
            self.reward = self.reward + 100
            if luggage not in self.success:
                self.success.append(luggage)
        else:
            self.failure.append(luggage)
            self.reward = self.reward - 1000

    def selectLuggage(self,_random=True):
        p1_lugg = self.possibleCombinations(passenger1.luggage)
        p2_lugg = self.possibleCombinations(passenger2.luggage)
        if _random:
            luggage = zip(p1_lugg,p2_lugg)
            return random.sample(list(luggage),self.batch_size)
        
    def possibleCombinations(self, luggage):
        return sum([list(map(list, itr.combinations(luggage, i))) for i in range(len(luggage) + 1)], [])

class Model3():
    def __init__(self,p1,p2):
        self.good_combination = np.array([])
        self.passenger1 = p1
        self.passenger2 = p2
        self.max_load = 500
        self.success = deque()
        self.failure = deque()
        self.reward = 0
        self.episode_reward = []
        self.batch_size = 5

    def Start(self, epoch):
        for episode in tqdm(range(epoch)):
            time.sleep(TIME_DELAY)
            self.train()
            self.episode_reward.append(self.reward)
        return self.success, self.reward,self.episode_reward

    def train(self):
        samples = self.selectLuggage()
        for lugg1,lugg2 in samples:
            if [lugg1,lugg2] not in self.failure:
                self.evaluate([lugg1,lugg2])
        return self.reward/self.batch_size

    def evaluate(self,luggage):
        p1_weight = np.sum(luggage[0]) + passenger1.passenger_weight
        p2_weight = np.sum(luggage[1]) + passenger2.passenger_weight
        totall = p1_weight+p2_weight
        if(totall <= self.max_load):
            self.reward = self.reward + 100
            if luggage not in self.success:
                self.success.append(luggage)
        else:
            self.failure.append(luggage)
            self.reward = self.reward - 1000

    def selectLuggage(self):
        count = 0
        x = np.random.randint(0,2)
        passenger1_luggage = []
        passenger2_luggage = []
        p1_lugg = self.possibleCombinations(passenger1.luggage)
        p2_lugg = self.possibleCombinations(passenger2.luggage)

        if(x == 0):
            passenger1_luggage = random.sample(list(p1_lugg),1)
            p1_weight = np.sum(passenger1_luggage)

            passenger2_sorted = np.sort(passenger2.luggage)[::-1]
            if(p1_weight <= 130):
                for i in range(3):
                    passenger2_luggage.append(passenger2_sorted[i])
                passenger2_luggage = [passenger2_luggage]
            elif(passenger1_luggage == []):
                passenger2_luggage = passenger2.luggage
            else:
                while True:
                    count=count+1
                    passenger2_luggage = random.sample(list(p2_lugg),1)
                    p2_weight = np.sum(passenger2_luggage)
                    if p1_weight + p2_weight <= 300 or count>23:
                        break
        elif(x == 1):
            passenger2_luggage = random.sample(list(p2_lugg),1)
            p2_weight = np.sum(passenger2_luggage)

            passenger1_sorted = np.sort(passenger1.luggage)[::-1]
            if(p2_weight <= 130):
                for i in range(3):
                    passenger1_luggage.append(passenger1_sorted[i])
                passenger1_luggage = [passenger1_luggage]
            elif(passenger2_luggage == []):
                passenger1_luggage = passenger1.luggage
            else:
                while True:
                    count=count+1
                    passenger1_luggage = random.sample(list(p1_lugg),1)
                    p1_weight = np.sum(passenger1_luggage)
                    if p1_weight + p2_weight <= 300 or count>23:
                        break

        luggage = zip(passenger1_luggage,passenger2_luggage)
        return luggage
        
    def possibleCombinations(self, luggage):
        return sum([list(map(list, itr.combinations(luggage, i))) for i in range(len(luggage) + 1)], [])

class Passenger():
    def __init__(self,lugg,weight):
        # lugg = np.append(lugg,0)
        self.luggage = lugg
        self.passenger_weight = weight
        self.ticket = 100

    def Weight(self):
        weight = np.sum(self.luggage)
        return weight + self.passenger_weight

    def Cost(self):
        cost = 10 * (self.Weight()-100)
        return cost + self.ticket

def plotResult(se1,se2,se3):
    y_data1 = se1
    y_data2 = se2
    y_data3 = se3
    x_data = range(EPOCH)
    plt.plot(x_data,y_data1,'r',x_data,y_data2,'y',x_data,y_data3,'g')
    plt.title('Finall Result of Three Models for {} Epoch'.format(EPOCH))
    plt.legend(['Model 1','Model 2','Model 3'], loc='lower right')
    plt.xlabel('Episode')
    plt.ylabel('Reward')
    plt.show()
    plt.plot(x_data,y_data1,'r')
    plt.legend(['Model 1'], loc='upper right')
    plt.show()
    plt.plot(x_data,y_data2,'y')
    plt.legend(['Model 2'], loc='lower right')
    plt.show()
    plt.plot(x_data,y_data3,'g')
    plt.legend(['Model 3'], loc='lower right')
    plt.show()
    plt.plot(x_data,y_data2,'y',x_data,y_data3,'g')
    plt.legend(['Model 2','Model 3'], loc='lower right')
    plt.show()

if __name__ == "__main__":
    lugg1 = np.array([20,80,30,100])
    passenger1_weight = 100
    lugg2 = np.array([50,90,30,50])
    passenger2_weight = 100
    passenger1 = Passenger(lugg1,passenger1_weight)
    passenger2 = Passenger(lugg2,passenger2_weight)

    print(tabulate([['1',passenger1.Weight(),passenger1.Cost()],['2',passenger2.Weight(),passenger2.Cost()]], headers=['Passenger','Totall Weight','Cost'],numalign="center"))
    print(div)

    print("Task 1")
    print(div)
    a = Model1(passenger1,passenger2)
    history1,reward1,episode_reward1 = a.Start(epoch=EPOCH)
    print("Optimal Luggage mix for {} Epoch = ".format(EPOCH))
    print('\n'.join(map(str,history1)))
    print("The Finall Reward is = ",reward1)
    print(div)

    print("Task 2")
    print(div)
    b = Model2(passenger1,passenger2)
    history2,reward2,episode_reward2 = b.Start(epoch=EPOCH)
    print("Optimal Luggage mix for {} Epoch = ".format(EPOCH))
    print('\n'.join(map(str,history2)))
    print("The Finall Reward is = ",reward2)
    print(div)

    print("Task 3")
    print(div)
    b = Model3(passenger1,passenger2)
    history3,reward3,episode_reward3 = b.Start(epoch=EPOCH)
    print("Optimal Luggage mix for {} Epoch = ".format(EPOCH))
    print('\n'.join(map(str,history3)))
    print("The Finall Reward is = ",reward3)
    print(div)

    plotResult(episode_reward1,episode_reward2,episode_reward3)
