import tensorflow as tf
import keras.backend as k
import numpy as np
import gym
from gym import wrappers

from ddpg import NormalizedEnv
from ddpg import DDPG
from ddpg import OrnsteinUhlenbeckProcess

ENV_NAME = "Walker2d-v2"    
NUM_EPISODES = 6
RUN_NAME = 'DDPG'
METRICS = False

if __name__ == "__main__":
    session = tf.Session()
    k.set_session(session)
    env = NormalizedEnv(gym.make(ENV_NAME))
    env = wrappers.Monitor(env, "recording", force=True)
    
    STATE_DIM = env.observation_space.shape[0]
    ACTION_DIM = env.action_space.shape[0]


    agent = DDPG(env, session, NUM_EPISODES, RUN_NAME, METRICS)
    noise = OrnsteinUhlenbeckProcess(mu=np.zeros(ACTION_DIM))

    # agent.load_weights('ddpg_{}_weights.h5f'.format(ENV_NAME))
    print("Weights Loaded!")

    for i_episode in range(NUM_EPISODES):
        current_state = env.reset()
        OrnsteinUhlenbeckProcess.reset_states(noise)
        action = env.action_space.sample()
        G = 0
        done = False
        while not done:
            # env.render()
            current_state = current_state.reshape((1, STATE_DIM))
            action = agent.act(current_state)

            action += noise() * max(0, agent.epsilon)
            action = np.clip(action, -1., 1.)
            action = action.reshape((1, ACTION_DIM))

            new_state, reward, done, _ = env.step(action)
            new_state = new_state.reshape((1, STATE_DIM))

            agent.remember(current_state, action, reward, new_state, done)
            agent.train()

            current_state = new_state
            G += reward
            if done:
                agent.episode_reward.append(G)
        if METRICS:
            loss,mse,mae = agent.metrics()
            print("Episode {} Reward {} | MSE {} | MAE {} | Loss {}".format(i_episode,G,mse,mae,loss))
        else:
            print("Episode {} Reward {} ".format(i_episode,G))
    
    agent.plot_metrics()
    
    agent.save_weights('ddpg_{}_weights.h5f'.format(ENV_NAME), overwrite=True)
    print("Weights saved!")
