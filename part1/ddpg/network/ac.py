import gym
import numpy as np 
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, Concatenate, Flatten
from keras.layers.merge import Add, Multiply
from keras.optimizers import Adam
import keras.backend as K

import tensorflow as tf

from collections import deque

class ActorCritic(object):
    """
    Role of Actor:
    Take the current state of environment, and determine the best action.

    Role of Critic:
    env:
    sess:
    learning_rate:
    epsilon: 
        epsilon is the decay for exploration and the noise applied to action 
        weighted by this value.
    epsilon_decay: 
        Inorder to not vanish our exploration, better to set this to zero.
        However, should be adjust this base on the case.
    gamma:
    tau:
        used to shifting from the prediction to target model 
    """
    def __init__(self, env, session):
        self.env  = env
        self.sess = session

        self.learning_rate = 1e-3
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.gamma = .95
        self.tau   = .125

        ###############################################################################
        #   I used model here to make a prediction based on action, and target model  # 
        #   to tracks what action is better to our model take.                        #
        ###############################################################################
        #   Init Actor Model:                                                         #
        ###############################################################################
        self.memory = deque(maxlen=2000)
        self.actor_state_input, self.actor_model = self.create_actor_model()
        _, self.target_actor_model = self.create_actor_model()

        self.actor_critic_grad = tf.placeholder(tf.float32, 
            [None, self.env.action_space.shape[0]]) # where we will feed de/dC (from critic)
		
        actor_model_weights = self.actor_model.trainable_weights
        self.actor_grads = tf.gradients(self.actor_model.output, 
            actor_model_weights, -self.actor_critic_grad) # dC/dA (from actor)
        grads = zip(self.actor_grads, actor_model_weights)
        self.optimize = tf.train.AdamOptimizer(self.learning_rate).apply_gradients(grads)

        ###############################################################################
        #   Init Critic Model:                                                        #
        ###############################################################################
        self.critic_state_input, self.critic_action_input, \
            self.critic_model = self.create_critic_model()
        _, _, self.target_critic_model = self.create_critic_model()

        self.critic_grads = tf.gradients(self.critic_model.output, 
            self.critic_action_input) # where we calcaulte de/dC for feeding above
		
		# Initialize for later gradient calculations
        self.sess.run(tf.global_variables_initializer())


    def create_actor_model(self):
        """
        Actor Model:
        """
        state_input = Input(shape=self.env.observation_space.shape)
        x = Dense(400, activation='relu')(state_input)
        x = Dense(300, activation='relu')(x)
        x = Dense(self.env.action_space.shape[0], activation='relu')(x)

        model = Model(input=state_input, output=x)
        print(model.summary())
        adam  = Adam(lr=self.learning_rate)
        model.compile(loss="mse", optimizer=adam, metrics=['mse', 'mae'])
        
        return state_input, model


    def create_critic_model(self):
        """
        Critic Model:
        """
        state_input = Input(shape=self.env.observation_space.shape, name="state_input")
        
        x = Dense(400, activation='relu')(state_input)
        x = Dense(300, activation='relu')(x)

        action_input = Input(shape=(self.env.action_space.shape[0],), name="action_input")
        a            = Dense(300)(action_input)

        x = Add()([x, a])
        x = Dense(300, activation='relu')(x)
        x = Dense(1, activation='relu')(x)
        model = Model(inputs=[state_input,action_input], output=x)
        print(model.summary())

        adam  = Adam(lr=self.learning_rate)
        model.compile(loss="mse", optimizer=adam, metrics=['mse', 'mae'])
        return state_input, action_input, model

