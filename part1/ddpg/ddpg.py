from network.ac import ActorCritic
import random
import keras
import os
import numpy as np 
import gym
import matplotlib.pyplot as plt


class DDPG(ActorCritic):
    """
    Deep Deterministic Policy Gradient:
    """
    def __init__(self, env, session, epoch, RUN_NAME, METRICS):
        ActorCritic.__init__(self, env, session)
        self.batch_size = 128
        self.mse = np.array([])
        self.mae = np.array([])
        self.loss = np.array([])
        self.episode_reward = []

        self.EPOCH = epoch
        self.metrics_flag = METRICS
        self.logger = keras.callbacks.TensorBoard(log_dir='logs/{}'.format(RUN_NAME), 
                                     write_graph=True)

    def remember(self, current_state, action, reward, new_state, done):
        self.memory.append([current_state, action, reward, new_state, done])

    def act(self, current_state):
        self.epsilon *= self.epsilon_decay
        if np.random.random() < self.epsilon:
            return self.env.action_space.sample()
        return self.actor_model.predict(current_state)

    def train(self):
        if len(self.memory) < self.batch_size:
            return
        
        samples = random.sample(self.memory, self.batch_size) 
        self.train_critic(samples)
        self.train_actor(samples)
        self.update_target()

    def update_target(self):
        self._update_actor_target()
        self._update_critic_target()

    def train_actor(self, samples):
        for sample in samples:
            current_state, action, reward, new_state, _ = sample
            predicted_action = self.actor_model.predict(current_state)
            grads = self.sess.run(self.critic_grads, feed_dict={
                self.critic_state_input: current_state,
                self.critic_action_input: predicted_action
            })[0]

            self.sess.run(self.optimize, feed_dict={
                self.actor_state_input: current_state,
                self.actor_critic_grad: grads
            })

    
    def train_critic(self, samples):
        for sample in samples:
            current_state, action, reward, new_state, done = sample
            if not done:
                target_action = self.target_actor_model.predict(new_state)
                future_reward = self.target_critic_model.predict(
                    [new_state, target_action])[0][0]
                reward += self.gamma * future_reward
            
            x=[current_state, action]
            y=np.array([reward]) 
            history = self.critic_model.fit(x=x, y=y, verbose=0, batch_size=self.batch_size)
            if self.metrics_flag:
                self.mse = np.append(self.mse, history.history['mean_squared_error'])
                self.mae = np.append(self.mae, history.history['mean_absolute_error'])
                self.loss = np.append(self.loss, history.history['loss'])

    def _update_actor_target(self):
        actor_model_weights  = self.actor_model.get_weights()
        actor_target_weights = self.target_actor_model.get_weights()
		
        for i in range(len(actor_target_weights)-1):
            actor_target_weights[i] = actor_model_weights[i] + (1-self.tau)*actor_target_weights[i]
        self.target_actor_model.set_weights(actor_target_weights)

    def _update_critic_target(self):
        critic_model_weights  = self.critic_model.get_weights()
        critic_target_weights = self.target_critic_model.get_weights()

        for i in range(len(critic_target_weights)):
            critic_target_weights[i] = critic_model_weights[i] + (1-self.tau)*critic_target_weights[i]
        self.target_critic_model.set_weights(critic_target_weights)		

    def save_weights(self, filepath, overwrite=False):
        filename, extension = os.path.splitext(filepath)
        actor_filepath = filename + '_actor' + extension
        critic_filepath = filename + '_critic' + extension
        self.actor_model.save_weights(actor_filepath, overwrite=overwrite)
        self.critic_model.save_weights(critic_filepath, overwrite=overwrite)

    def load_weights(self, filepath):
        filename, extension = os.path.splitext(filepath)
        actor_filepath = filename + '_actor' + extension
        critic_filepath = filename + '_critic' + extension
        self.actor_model.load_weights(actor_filepath)
        self.critic_model.load_weights(critic_filepath)
        self.update_target_models_hard()

    def update_target_models_hard(self):
        self.target_critic_model.set_weights(self.critic_model.get_weights())
        self.target_actor_model.set_weights(self.actor_model.get_weights())

    def metrics(self):
        loss_average = np.mean(self.loss)
        mse_average = np.mean(self.mse)
        mae_average = np.mean(self.mae)
        return loss_average, mse_average, mae_average

    def plot_metrics(self):

        # y_data1 = self.loss
        # y_data2 = self.mse
        # y_data3 = self.mae
        y_data3 = self.episode_reward
        x_data = range(self.EPOCH)

        # plt.plot(x_data,y_data1,'r',x_data,y_data2,'g',x_data,y_data3,'b')
        plt.plot(x_data,y_data3,'b')
        plt.title('Finall Result of Three Models for {} Epoch'.format(self.EPOCH))
        # plt.legend(['Model1','Model2','Model3'], loc='bottom left')
        plt.xlabel('Episode')
        plt.ylabel('Episode Reward')
        plt.show()
        # plt.ion()

        # # # Plot training & validation accuracy values
        # plt.plot(self.mse)
        # # plt.plot(history.history['val_acc'])
        # plt.title('Model accuracy')
        # plt.ylabel('Mean Squared Error')
        # plt.xlabel('Epoch')
        # plt.legend(['Train', 'Test'], loc='upper left')
        # plt.draw()
        # plt.pause(0.0001)
        # plt.clf()

        # # Plot training & validation loss values
        # plt.plot(self.mae)
        # # plt.plot(history.history['val_loss'])
        # plt.title('Mean Absolute Error')
        # plt.ylabel('Loss')
        # plt.xlabel('Epoch')
        # plt.legend(['Train', 'Test'], loc='upper left')
        # plt.draw()
        # plt.pause(0.0001)
        # plt.clf()
class OrnsteinUhlenbeckProcess:
    """
    Adding the noise parameteres to environment for exploration 
    The implementation is based on
    http://math.stackexchange.com/questions/1287634/implementing-ornstein-uhlenbeck-in-matlab
    """
    def __init__(self, mu=0, sigma=0.2, theta=.15, dt=1e-2, x0=None):
        self.sigma = sigma
        self.theta = theta
        self.mu = mu
        self.dt = dt
        self.x0 = x0
        self.reset_states()

    def __call__(self):
        x = self.x_prev + self.theta * (self.mu - self.x_prev) * self.dt + self.sigma * np.sqrt(self.dt) * np.random.normal(size=self.mu.shape)
        self.x_prev = x
        return x

    def reset_states(self):
        self.x_prev = self.x0 if self.x0 is not None else np.zeros_like(self.mu)

class NormalizedEnv(gym.ActionWrapper):
    """
    Normalizing the environment by overloding the corresponding
    functions
    """
    def action(self, action):
        act_k = (self.action_space.high - self.action_space.low)/ 2.
        act_b = (self.action_space.high + self.action_space.low)/ 2.
        return act_k * action + act_b

    def reverse_action(self, action):
        act_k_inv = 2./(self.action_space.high - self.action_space.low)
        act_b = (self.action_space.high + self.action_space.low)/ 2.
        return act_k_inv * (action - act_b)